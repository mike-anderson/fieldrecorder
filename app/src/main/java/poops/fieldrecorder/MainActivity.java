package poops.fieldrecorder;

import android.support.v7.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.media.MediaRecorder;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static poops.fieldrecorder.AudioState.*;


//http://www.tutorialspoint.com/android/android_audio_capture.htm
//public class MainActivity extends Activity {
public class MainActivity extends AppCompatActivity {
    private static ImageButton record;
    private static ImageButton play;
    private static ImageButton stop;

    //ImageView logo;
    private static pl.droidsonroids.gif.GifTextView logo;
    private static TextView counter;
    private static Long recordStartTime = 0L;

    private String appName = null;
    private String outputFilePath = null;

    private AudioChannels audioChannels = AudioChannels.MONO;
    //TODO Add check for API?
    private AudioFormats audioFormat = AudioFormats.AAC;

    //Player/Recorder
    private static MediaPlayer myAudioPlayer;
    private static MediaRecorder myAudioRecorder;
    //static AudioRecord audioRecord; //maybe be a fix for timing issue http://stackoverflow.com/questions/15955958/android-audiorecord-to-server-over-udp-playback-issues

    //Recording stats
    private static AudioState audioState = AudioState.STOPPED;

    //Timer
    private long startTime;
    private long elapsedTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //logo replacement setup
        logo = (pl.droidsonroids.gif.GifTextView)findViewById(R.id.logo);
        logo.setBackgroundResource(R.drawable.icon);

        //counter setup
        counter = (TextView)findViewById(R.id.counter);
        counter.setText(getString(R.string.counter));

        appName = getString(R.string.app_name).replace(" ", "_");
        createAppStorageDirectory();

        //Set image buttons
        record = (ImageButton)findViewById(R.id.button1);
        play = (ImageButton)findViewById(R.id.button2);
        stop = (ImageButton)findViewById(R.id.button3);

        //Initialize button states
        record.setEnabled(true);
        stop.setEnabled(false);
        play.setEnabled(false);

        //Setup monitor
        ScheduledExecutorService myScheduledExecutorService = Executors.newScheduledThreadPool(1);
        myScheduledExecutorService.scheduleWithFixedDelay(
                new Runnable(){
                    @Override
                    public void run() {
                        monitorHandler.sendMessage(monitorHandler.obtainMessage());
                    }
                },
                200, //initialDelay
                100, //delay
                TimeUnit.MILLISECONDS
        );

        //Record
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (myAudioPlayer != null && myAudioPlayer.isPlaying()) {
                        audioState = STOPPED;
                        myAudioPlayer.stop();
                        Toast.makeText(getApplicationContext(), "Playback stopped", Toast.LENGTH_LONG).show();
                    }

                    if (!audioState.equals(RECORD)) {
                        audioState = RECORD;

                        //Set up recorder
                        myAudioRecorder = new MediaRecorder();
                        myAudioRecorder.setAudioChannels(audioChannels.getValue());

                        //http://stackoverflow.com/questions/16536031/record-only-callers-voice-in-android
                        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                        //myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL); //Record phone calls (Caller audio + local voice mic)
                        //myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.VOICE_DOWNLINK); //Record phone calls (Caller audio only)
                        //myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.VOICE_DOWNLINK); //Record phone calls (local voice mic audio only)

                        switch (audioFormat) {
                            case THREE_GPP:
                                myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                                myAudioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                                break;
                            case AAC:
                                myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                                myAudioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                                break;
                        }

                        setOutputFilePath();
                        myAudioRecorder.setOutputFile(outputFilePath);

                        myAudioRecorder.prepare();
                        myAudioRecorder.start();

                        //Set logo to animated
                        //logo.setBackgroundResource(R.drawable.logo);

                        //Set counter recorder start time
                        recordStartTime = System.currentTimeMillis();
                    }
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                setButtonStates();

                Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_LONG).show();
            }
        });

        //Stop
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //Set logo to non-animated
                    //logo.setBackgroundResource(R.drawable.icon);

                    if (myAudioRecorder != null && audioState.equals(RECORD)) {
                        //Thread.sleep(1*1000); // Last second of recording gets cut-off, cheap fix until I can figure out a better solution.
                        //Thread.sleep((Long)((System.currentTimeMillis() - recordStartTime)) / 10);

                        myAudioRecorder.stop();
                        myAudioRecorder.release();

                        audioState = STOPPED;

                        Toast.makeText(getApplicationContext(), "Recording stopped", Toast.LENGTH_LONG).show();
                    } else if (myAudioPlayer != null && myAudioPlayer.isPlaying()) {
                        audioState = STOPPED;
                        myAudioPlayer.stop();

                        Toast.makeText(getApplicationContext(), "Playback stopped", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Nothing to stop", Toast.LENGTH_LONG).show();
                    }
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }/* catch (InterruptedException e) {
                    e.printStackTrace();
                }*/

                setButtonStates();
            }
        });

        //Play
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) throws IllegalArgumentException, SecurityException, IllegalStateException {
                try {
                    if (myAudioPlayer != null) {
                        if (myAudioPlayer.isPlaying()) {
                            audioState = STOPPED; //audioState.setState(AudioState.STOPPED);
                            myAudioPlayer.stop();
                        }
                        myAudioPlayer.release();
                        myAudioPlayer = null;
                    }

                    if (audioState.equals(STOPPED) && myAudioPlayer == null) {
                        myAudioPlayer = new MediaPlayer();

                        try {
                            myAudioPlayer.setDataSource(outputFilePath);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        try {
                            if (myAudioPlayer != null) {
                                myAudioPlayer.prepare();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (myAudioPlayer != null && !myAudioPlayer.isPlaying()) {
                            audioState = PLAYBACK;
                            myAudioPlayer.start();
                            //logo.setBackgroundResource(R.drawable.logo);
                        }

                        //System.out.println("\n*** myAudioPlayer.getCurrentPosition() = " + myAudioPlayer.getCurrentPosition() + "\n");
                        //System.out.println("\n*** myAudioPlayer.getDuration() = " + myAudioPlayer.getDuration() + "\n");

                        //http://stackoverflow.com/questions/9962998/oncompletion-listener-with-mediaplayer
                        myAudioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                if (myAudioPlayer != null) {
                                    /*playbackOn = false;*/
                                    audioState = STOPPED;

                                    //Set logo to non-animated
                                    //logo.setBackgroundResource(R.drawable.icon);
                                    setButtonStates();

                                    Toast.makeText(getApplicationContext(), "Playback stopped", Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                        System.out.println("\n*** myAudioPlayer.getCurrentPosition() = " + myAudioPlayer.getCurrentPosition() + "\n");

                        Toast.makeText(getApplicationContext(), "Playing audio", Toast.LENGTH_LONG).show();
                    } else if (audioState.equals(RECORD)) {
                        Toast.makeText(getApplicationContext(), "Still recording audio, press stop to end recording", Toast.LENGTH_LONG).show();
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }

                setButtonStates();
            }
        });


    }

    //http://android-er.blogspot.ca/2012/06/display-playing-time-for-mediaplayer.html
    //TODO: find a better way to do this? Is this the most resource effective way?
    static Handler monitorHandler = new Handler(){

        @Override
        public void handleMessage(Message msg) {

            if (audioState.equals(PLAYBACK)) { //playback
                if (myAudioPlayer != null && myAudioPlayer.isPlaying()) {
                    int currentPosition = myAudioPlayer.getCurrentPosition();

                    Locale locale = new Locale("US");
                    String counterTime = (new SimpleDateFormat("mm:ss:SS", locale)).format(new Time(currentPosition));
                    counter.setText(counterTime);
                }
            } else if (audioState.equals(RECORD)){ //recording
                if (myAudioRecorder != null) {
                    //http://stackoverflow.com/questions/7308070/how-to-set-time-calculation-in-mediarecorder
                    //System.out.println("*** recordStartTime=" + recordStartTime +", System.currentTimeMillis()=" + System.currentTimeMillis() + ", Current-recordStartTime=" + ((System.currentTimeMillis() - recordStartTime)));

                    Long currentPosition = (Long) ((System.currentTimeMillis() - recordStartTime));
                    Locale locale = new Locale("US");
                    String counterTime = (new SimpleDateFormat("mm:ss:SS", locale)).format(new Time(currentPosition));
                    counter.setText(counterTime);
                }
            }
            //TODO add VU meter
            //http://stackoverflow.com/questions/4154023/range-of-values-for-getmaxamplitude
            //Input levels, 0 to 32768
            //myAudioRecorder.getMaxAmplitude()
            //counter.setText("*** Max Amplitude = " + myAudioRecorder.getMaxAmplitude());

        }
    };

    public void createAppStorageDirectory() {
        // create a File object for the parent directory
        File recordingDirectory = new File(Environment.getExternalStorageDirectory() + "/" + appName + "/");

        // have the object build the directory structure, if needed.
        try {
            if (recordingDirectory.mkdirs()) {
                System.out.println("\n*** Directory " + appName + "created.\n");
            } else {
                System.out.println("\n*** Directory " + appName + " not created.\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String setOutputFileName() {
        //Create output file name
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        String tempTimestamp = timestamp.toString().replace(" ", "_").replace(":", "-");
        String fileName = appName.concat(tempTimestamp);

        System.out.println("\n*** outputFileName = " + fileName + "\n");

        return fileName;
    }

    private void setOutputFilePath() {
        //Create output file path
        String tempAppName = setOutputFileName();
        outputFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + appName + "/" + tempAppName +".3gp";

        System.out.println("\n*** outputFilePath = " + outputFilePath + "\n");
    }

    public static void setButtonStates() {
        switch (audioState) {
            case RECORD:
                record.setEnabled(false);
                play.setEnabled(false);
                stop.setEnabled(true);
                /*record.setBackgroundResource(R.drawable.record_state1); //Set record button state
                stop.setBackgroundResource(R.drawable.stop_state0); //Set stop button state
                play.setBackgroundResource(R.drawable.play_state0); //Set play button state
                logo.setBackgroundResource(R.drawable.logo_state1); //Set logo to animated*/
                break;
            case PLAYBACK:
                record.setEnabled(false);
                play.setEnabled(false);
                stop.setEnabled(true);
                /*record.setBackgroundResource(R.drawable.record_state0); //Set record button state
                stop.setBackgroundResource(R.drawable.stop_state0); //Set stop button state
                play.setBackgroundResource(R.drawable.play_state1); //Set play button state
                logo.setBackgroundResource(R.drawable.logo_state1); //Set logo to animated*/
                break;
            case STOPPED:
                record.setEnabled(true);
                play.setEnabled(true);
                stop.setEnabled(false);
                /*record.setBackgroundResource(R.drawable.record_state0); //Set record button state
                stop.setBackgroundResource(R.drawable.stop_state1); //Set stop button state
                play.setBackgroundResource(R.drawable.play_state0); //Set play button state
                logo.setBackgroundResource(R.drawable.logo_state0); //Set logo to static*/
                break;
            case PAUSED_PLAYBACK:
                record.setEnabled(false);
                play.setEnabled(true);
                stop.setEnabled(true);
                /*record.setBackgroundResource(R.drawable.record_state0); //Set record button state
                stop.setBackgroundResource(R.drawable.stop_state0); //Set stop button state
                play.setBackgroundResource(R.drawable.play_state0); //Set play button state
                logo.setBackgroundResource(R.drawable.logo_state0); //Set logo to static*/
            //Add the rest as/if added
            default:
                record.setEnabled(true);
                play.setEnabled(true);
                stop.setEnabled(true);
                /*record.setBackgroundResource(R.drawable.record_state0); //Set record button state
                stop.setBackgroundResource(R.drawable.stop_state1); //Set stop button state
                play.setBackgroundResource(R.drawable.play_state0); //Set play button state
                logo.setBackgroundResource(R.drawable.logo_state0); //Set logo to static*/
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Commented out, not needed ATM

        //Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //Commented out, not needed ATM

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }
}