package poops.fieldrecorder;

/**
 * Created by Mike on 2016-06-19.
 */
public enum AudioState {
    RECORD (0),
    PLAYBACK (1),
    STOPPED(2),
    PAUSED_RECORD (3),
    PAUSED_PLAYBACK (4),
    SEEK_FW (5),
    SEEK_RV (6),
    NEXT (7),
    PREVIOUS (8);

    private int state;

    AudioState(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

    public int getState(String name) {
        return AudioState.valueOf(name).getState();
    }

    public void setState(String name) {
        this.state = AudioState.valueOf(name).getState();
    }

    public void setState(AudioState audioState) {
        this.state = audioState.getState();
    }

}
