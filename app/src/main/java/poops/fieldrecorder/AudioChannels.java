package poops.fieldrecorder;

/**
 * Created by Mike on 2016-06-06.
 */
public enum AudioChannels {
    MONO (1),
    STEREO (2);

    private int audioChannels;

    AudioChannels(int audioChannels) {
        this.audioChannels = audioChannels;
    }

    public int getValue() {
        return audioChannels;
    }
}
