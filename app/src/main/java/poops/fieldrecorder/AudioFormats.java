package poops.fieldrecorder;

/**
 * Created by Mike on 2016-06-06.
 */
public enum AudioFormats {
    THREE_GPP (1),
    AAC (2);

    private int audioFormats;

    AudioFormats(int audioFormats) {
        this.audioFormats = audioFormats;
    }

    public int getValue() {
        return audioFormats;
    }
}

